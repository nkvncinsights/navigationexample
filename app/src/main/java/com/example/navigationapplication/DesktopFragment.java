package com.example.navigationapplication;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class DesktopFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_desktop, container, false);
        Button desktopButton = rootview.findViewById(R.id.backToProgress);
        desktopButton.setOnClickListener(Navigation.createNavigateOnClickListener(
                R.id.action_desktopFragment_to_progressFragment, null));
        return rootview;
    }
}

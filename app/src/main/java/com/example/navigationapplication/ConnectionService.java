package com.example.navigationapplication;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

public class ConnectionService extends Service {
    private final IBinder binder = new ConnectionBinder();

    public ConnectionService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class ConnectionBinder extends Binder {
        ConnectionService getService() {
            return ConnectionService.this;
        }
    }
}

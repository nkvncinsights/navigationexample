package com.example.navigationapplication;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class ProgressFragment extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.progress_view_fragment, container, false);
        Button desktopButton = rootview.findViewById(R.id.desktopbutton);
        desktopButton.setOnClickListener(Navigation.createNavigateOnClickListener(
                R.id.action_progressFragment_to_desktopFragment, null));
        return rootview;
    }

}
